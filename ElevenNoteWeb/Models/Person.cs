﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevenNoteWeb.Models
{
    public class Person
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public Person()
        {
        }

        public Person(string f, string l)
        {
            FirstName = f;
            LastName = l;
        }
    }

}
