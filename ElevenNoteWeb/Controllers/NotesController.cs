﻿using Contracts;
using ElevenNoteWeb.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElevenNoteWeb.Controllers
{
    [Authorize]
    public class NotesController : Controller
    {

        private DataManagement _data = new DataManagement();

        /// <summary>
        /// Gets the ID of the current user.
        /// </summary>
        /// <returns></returns>
        private Guid GetUserId()
        {
            using (var c = new ApplicationDbContext())
            {
                var userId = c.Users.Where(w => w.UserName == User.Identity.Name).Select(s => s.Id).FirstOrDefault();

                //var id = (
                //    from user in c.Users 
                //    where user.UserName == User.Identity.Name 
                //    select user.Id).FirstOrDefault();

                return new Guid(userId);
            }
        }

        // GET: Notes
        public ActionResult Index()
        {
            var notes = _data.GetAll(GetUserId());
            return View(notes);
        }

        [HttpGet]
        [ActionName("Create")]
        public ActionResult CreateGet()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName("Create")]
        public ActionResult CreatePost(NoteDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserID = GetUserId().ToString();
                _data.Add(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult EditGet(int? id)
        {
            if (id == null) return HttpNotFound(); // make sure they passed an ID
            var model = _data.GetById(id.Value, GetUserId()); // get the note that matches the ID
            if (model != null)
            {
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ActionName("Edit")]
        public ActionResult EditPost(NoteDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserID = GetUserId().ToString();
                _data.Update(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int? id)
        {
            if (id == null) return HttpNotFound(); // make sure they passed an ID
            var model = _data.GetById(id.Value, GetUserId()); // get the note that matches the ID
            if (model != null)
            {
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            // Attempt to retrieve the Note so they can preview before deleting.
            var model = _data.GetById(id.Value, GetUserId());
            if (model != null)
            {
                _data.Delete(id.Value, GetUserId());
                return RedirectToAction("Index");
            }
            else
            {
                return HttpNotFound(); // returns a 404
            }
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return HttpNotFound(); // make sure they passed an ID
            var model = _data.GetById(id.Value, GetUserId()); // get the note that matches the ID
            if (model != null)
            {
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}