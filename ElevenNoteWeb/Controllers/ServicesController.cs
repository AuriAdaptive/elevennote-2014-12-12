﻿using ElevenNoteWeb.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElevenNoteWeb.Controllers
{
    [Authorize]
    public class ServicesController : Controller
    {
        private DataManagement _data = new DataManagement();

        /// <summary>
        /// Gets the ID of the current user.
        /// </summary>
        /// <returns></returns>
        private Guid GetUserId()
        {
            using (var c = new ApplicationDbContext())
            {
                var userId = c.Users.Where(w => w.UserName == User.Identity.Name).Select(s => s.Id).FirstOrDefault();

                //var id = (
                //    from user in c.Users 
                //    where user.UserName == User.Identity.Name 
                //    select user.Id).FirstOrDefault();

                return new Guid(userId);
            }
        }

        [HttpPost]
        public JsonResult GetNotesForUser()
        {
            var notes = _data.GetAll(GetUserId());

            return Json(notes);
        }
    }
}