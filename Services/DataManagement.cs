﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Services
{
    public class DataManagement
    {
        public void TestFunction()
        {
            using (var context = new DataAccess.AdventureWorksEntities1())
            {
                using (var t = new TransactionScope())
                {
                    try
                    {
                        var results = (from r in context.Customers where r.CustomerType == "Corporate" select r).ToList();
                        context.SaveChanges();
                        t.Complete();
                    }
                    catch
                    {
                        // Continue.
                    }
                }
            }
        }

        public int Add(NoteDetailViewModel model)
        {
            if (model.ID > 0 || model.UserID == null)
            {
                throw new ArgumentException("Either User ID was null or model ID was greater than zero.");
            }

            using (var c = new DataAccess.ElevenNoteContext())
            {
                var note = new DataAccess.Note()
                {
                    Content = model.Content,
                    DateCreated = DateTime.Now,
                    Title = model.Title,
                    UserId = new Guid(model.UserID)
                };

                c.Notes.Add(note);

                c.SaveChanges();

                return note.Id;
            }
        }

        public bool Update(NoteDetailViewModel model)
        {
            if (model.ID < 0 || model.UserID == null) return false;

            using (var c = new DataAccess.ElevenNoteContext())
            {
                var note = c.Notes.Where(w => w.Id == model.ID && w.UserId.ToString() == model.UserID).FirstOrDefault();

                if (note == null) return false; // make sure they own the note

                note.Title = model.Title;
                note.Content = model.Content;
                note.DateModified = DateTime.Now;

                c.SaveChanges();

                return true;
            }
        }

        public bool Delete(int id, Guid userId)
        {
            if (id < 0) return false;

            using (var c = new DataAccess.ElevenNoteContext())
            {
                var note = c.Notes.Where(w => w.Id == id && w.UserId == userId).FirstOrDefault();

                if (note == null) return false; // make sure they own the note

                c.Notes.Remove(note);
                c.SaveChanges();

                return true;
            }
        }

        public List<NoteListViewModel> GetAll(Guid userId)
        {
            using (var c = new DataAccess.ElevenNoteContext())
            {
                return (from r in c.Notes
                        where r.UserId == userId
                        select new NoteListViewModel()
                        {
                            DateCreated = r.DateCreated,
                            DateModified = r.DateModified,
                            ID = r.Id,
                            Title = r.Title
                        }).ToList();
            }
        }

        public NoteDetailViewModel GetById(int id, Guid userId)
        {
            // Make sure ID appears OK.
            if (id < 0) return null;

            using (var c = new DataAccess.ElevenNoteContext())
            {
                return (
                    from r in c.Notes
                    where r.Id == id && r.UserId == userId
                    select new NoteDetailViewModel()
                    {
                        Content = r.Content,
                        DateCreated = r.DateCreated,
                        DateModified = r.DateModified,
                        ID = r.Id,
                        Title = r.Title,
                        UserID = r.UserId.ToString()
                    }).FirstOrDefault();
            }
        }

    }
}
