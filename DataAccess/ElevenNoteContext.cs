﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ElevenNoteContext : DbContext, IDisposable
    {
        #region Constructor

        public ElevenNoteContext()
            : base("DefaultConnection")
        {
            // Nothing to do here.
        }

        #endregion

        #region Datasets and other database config

        public DbSet<Note> Notes { get; set; }

        #endregion

    }
}
